// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"errors"
	"math"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/log"
)

var (
	// errFutureMessage is returned when current view is earlier than the
	// view of the received message.
	errFutureMessage = errors.New("future message")
	// errOldMessage is returned when the received message's view is earlier
	// than current view.
	errOldMessage = errors.New("old message")
	// errInvalidMessage is returned when the message is malformed.
	errInvalidMessage = errors.New("invalid message")
)

// CoreStart start core
func (fb *FastBFT) CoreStart() error {
	log.Info("FastBFT Start")

	fb.lockset = NewLockset()
	// Start a new round from last round + 1
	fb.startNewRound(common.Big0)

	// Tests will handle events itself, so we have to make subscribeEvents()
	// be able to call in test.
	fb.subscribeEvents()
	go fb.handleEvents()

	return nil
}

func (fb *FastBFT) startNewRound(round *big.Int) {
	fb.currentHeight = new(big.Int).Add(fb.currentBlock().Number(), common.Big1)
	fb.currentRound = round
	log.Info("Start New Round", "height", fb.currentHeight, "round", fb.currentRound, "lastProposer", fb.lastProposer())
	fb.valSet.CalcLeader(fb.lastProposer(), round)
	fb.newTimer()
	fb.sendProposal(round)
}

func (fb *FastBFT) sendProposal(round *big.Int) {
	fb.lockset.Clear()
	log.Info("Leader", "leader", fb.valSet.GetLeader(), "signer", fb.signer)
	if fb.faultNode == 1 && fb.valSet.IsLeader(fb.signer) {
		log.Info("You are the leader, sending proposal")
		// round > 0
		if round.Cmp(common.Big0) == 1 && fb.IsValid(fb.previosLockset) {
			locked, lockedBlock := fb.IsBlockLocked(fb.previosLockset)
			// there is a block get more than 40% votes in last round
			if locked {
				log.Info("block is locked")
				proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, lockedBlock)
				fb.FaultBroadcast(fb.valSet, proposalMsg, proposal, fb.makeFaultProposal())
			} else if !locked && fb.currentPrepare.Block != nil {
				log.Info("block is not locked")
				proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepare.Block)
				fb.FaultBroadcast(fb.valSet, proposalMsg, proposal, fb.makeFaultProposal())
			}
		// round = 0
		} else if round.Cmp(common.Big0) == 0 && fb.currentPrepare.Block != nil {
			log.Info("round = 0")
			proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepare.Block)
			fb.FaultBroadcast(fb.valSet, proposalMsg, proposal, fb.makeFaultProposal())
		}
	} else if fb.valSet.IsLeader(fb.signer) {
		log.Info("You are the leader, sending proposal")
		// round > 0
		if round.Cmp(common.Big0) == 1 && fb.IsValid(fb.previosLockset) {
			locked, lockedBlock := fb.IsBlockLocked(fb.previosLockset)
			// there is a block get more than 40% votes in last round
			if locked {
				log.Info("block is locked")
				proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, lockedBlock)
				fb.Broadcast(fb.valSet, proposalMsg, proposal)
			} else if !locked && fb.currentPrepare.Block != nil {
				log.Info("block is not locked")
				proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepare.Block)
				fb.Broadcast(fb.valSet, proposalMsg, proposal)
			}
		// round = 0
		} else if round.Cmp(common.Big0) == 0 && fb.currentPrepare.Block != nil {
			log.Info("round = 0")
			proposal := NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepare.Block)
			fb.Broadcast(fb.valSet, proposalMsg, proposal)
		}
	} else {
		log.Info("You are not leader, waiting for proposal")
		fb.processPendingProposal()
	}
}

// CoreStop stop core
func (fb *FastBFT) CoreStop() error {
	fb.stopVoteTimer()
	fb.stopCommitTimer()
	fb.unsubscribeEvents()

	// Make sure the handler goroutine exits
	fb.handlerWg.Wait()
	return nil
}

// Subscribe both internal and external events
func (fb *FastBFT) subscribeEvents() {
	fb.events = fb.EventMux().Subscribe(
		// external events
		Prepare{},
		Proposal{},
		Vote{},
	)
	fb.timeoutSub = fb.EventMux().Subscribe(
		timeoutEvent{},
	)
	fb.finalCommittedSub = fb.EventMux().Subscribe(
		FinalCommittedEvent{},
	)
}

// Unsubscribe all events
func (fb *FastBFT) unsubscribeEvents() {
	fb.events.Unsubscribe()
	fb.timeoutSub.Unsubscribe()
}

func (fb *FastBFT) stopVoteTimer() {
	if fb.voteTimer != nil {
		fb.voteTimer.Stop()
	}
}

func (fb *FastBFT) stopCommitTimer() {
	if fb.commitTimer != nil {
		fb.commitTimer.Stop()
	}
}

func (fb *FastBFT) newTimer() {
	fb.newVoteTimer()
	fb.newCommitTimer()
}

func (fb *FastBFT) newVoteTimer() {
	fb.stopVoteTimer()

	// set timeout based on the round number
	timeout := time.Duration(1000) * time.Millisecond
	round := fb.currentRound.Uint64()
	if round > 0 {
		timeout = timeout * time.Duration(math.Pow(2, float64(round)))
		// timeout = timeout * time.Duration(math.Pow(2, 0))
	}

	fb.voteTimer = time.AfterFunc(timeout, func() {
		fb.EventMux().Post(timeoutEvent{1, round})
	})
}

func (fb *FastBFT) newCommitTimer() {
	fb.stopCommitTimer()

	// set timeout based on the round number
	timeout := time.Duration(2000) * time.Millisecond
	round := fb.currentRound.Uint64()
	if round > 0 {
		timeout = timeout * time.Duration(math.Pow(2, float64(round)))
		// timeout = timeout * time.Duration(math.Pow(2, 0))
	}

	fb.commitTimer = time.AfterFunc(timeout, func() {
		fb.EventMux().Post(timeoutEvent{2, round})
	})
}

func (fb *FastBFT) lastProposer() (common.Address) {
	block := fb.currentBlock()

	var proposer common.Address
	if block.Number().Cmp(common.Big0) > 0 {
		var err error
		proposer, err = fb.Author(block.Header())
		if err != nil {
			log.Error("Failed to get block proposer", "err", err)
			return common.Address{}
		}
	}

	// Return header only block here since we don't need block body
	return proposer
}