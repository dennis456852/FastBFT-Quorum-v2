// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"crypto/ecdsa"
	"errors"
	"fmt"
	"math/big"
	"strings"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/consensus"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/state"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto/sha3"
	"github.com/ethereum/go-ethereum/ethdb"
	"github.com/ethereum/go-ethereum/event"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/p2p"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/ethereum/go-ethereum/rpc"
	lru "github.com/hashicorp/golang-lru"
	"gopkg.in/karalabe/cookiejar.v2/collections/prque"
)

// Various error messages to mark blocks invalid. These should be private to
// prevent engine specific errors from being referenced in the remainder of the
// codebase, inherently breaking if the engine is swapped out. Please put common
// error types into the consensus package.
var (
	// ErrStoppedEngine is returned if the engine is stopped
	ErrStoppedEngine = errors.New("stopped engine")
	// errUnknownBlock is returned when the list of signers is requested for a block
	// that is not part of the local blockchain.
	errUnknownBlock = errors.New("unknown block")
	// errInvalidDifficulty is returned if the difficulty of a block is not 1
	errInvalidDifficulty = errors.New("invalid difficulty")
	// errInvalidUncleHash is returned if a block contains an non-empty uncle list.
	errInvalidUncleHash = errors.New("non empty uncle hash")
	// errNotLeader
	errNotLeader = errors.New("not leader")
	// ErrUnauthorizedAddress is returned when given address cannot be found in
	// current validator set.
	ErrUnauthorizedAddress = errors.New("unauthorized address")
	// errDecodeFailed is returned when decode message fails
	errDecodeFailed = errors.New("fail to decode fastbft message")
)

var (
	defaultDifficulty = big.NewInt(1)
	nilUncleHash      = types.CalcUncleHash(nil) // Always Keccak256(RLP([])) as uncles are meaningless outside of PoW.
	emptyNonce        = types.BlockNonce{}
	now               = time.Now
	extraVanity 	  = 32 // Fixed number of extra-data prefix bytes reserved for signer vanity
	extraSeal   	  = 65 // Fixed number of extra-data suffix bytes reserved for signer seal
)

const (
	inmemoryPeers    = 40
	inmemoryMessages = 1024
)

// Validator interface
type Validator interface {
	// Address returns address
	Address() common.Address

	// String representation of Validator
	String() string
}

// Validators type
type Validators []Validator

func (slice Validators) Len() int {
	return len(slice)
}

func (slice Validators) Less(i, j int) bool {
	return strings.Compare(slice[i].String(), slice[j].String()) < 0
}

func (slice Validators) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

// ValidatorSet interface
type ValidatorSet interface {
	// Calculate the leader
	CalcLeader(lastLeader common.Address, round *big.Int)
	// Return the validator size
	Size() int
	// Return the validator array
	List() []Validator
	// Get validator by index
	GetByIndex(i uint64) Validator
	// Get validator by given address
	GetByAddress(addr common.Address) (int, Validator)
	// Get current leader
	GetLeader() Validator
	// Check whether the validator with given address is a leader
	IsLeader(address common.Address) bool
	// Update Leader
	UpdateLeader(round *big.Int)
	// Add validator
	AddValidator(address common.Address) bool
	// Remove validator
	RemoveValidator(address common.Address) bool
	// Copy validator set
	Copy() ValidatorSet
	// Get the maximum number of faulty nodes
	F() int
}

// FastBFT is the Two-Step BFT consensus engine
type FastBFT struct {
	signer 				common.Address // Ethereum address of the signing key
	// signFn 			SignerFn       // Signer function to authorize hashes with
	lock   				sync.RWMutex   // Protects the signer fields

	fastbftEventMux 	*event.TypeMux
	coreMu          	sync.RWMutex
	coreStarted     	bool

	db           		ethdb.Database
	chain        		consensus.ChainReader
	currentBlock 		func() *types.Block
	hasBadBlock  		func(hash common.Hash) bool

	logger 				log.Logger
	valSet 				ValidatorSet

	handlerWg 			*sync.WaitGroup

	currentHeight 		*big.Int
	currentRound  		*big.Int

	// the channels for fastbft engine notifications
	commitCh          	chan *types.Block
	proposedBlockHash 	common.Hash
	sealMu            	sync.Mutex

	// event subscription for ChainHeadEvent event
	broadcaster    		consensus.Broadcaster
	recentMessages 		*lru.ARCCache // the cache of peer's messages
	knownMessages  		*lru.ARCCache // the cache of self messages

	events 				*event.TypeMuxSubscription
	timeoutSub      	*event.TypeMuxSubscription
	finalCommittedSub	*event.TypeMuxSubscription
	
	lockset 			*Lockset
	previosLockset		*Lockset

	voteTimer			*time.Timer
	commitTimer			*time.Timer

	pendingProposals   	*prque.Prque
	pendingProposalsMu 	*sync.Mutex
	pendingVotes   		*prque.Prque
	pendingVotesMu 		*sync.Mutex

	currentProposal 	Proposal
	currentPrepare 		Prepare
	currentPrepareFault Prepare

	// 0: nothing, 1: send different proposal, 2: send different vote
	faultNode			int
}

// New creates a FastBFT consensus engine with the initial
// signers set to the ones provided by the user.
//func New(config *params.FastBFTConfig, privateKey *ecdsa.PrivateKey, db ethdb.Database) *FastBFT {
func New(privateKey *ecdsa.PrivateKey, db ethdb.Database, faultNode int) *FastBFT {
	recentMessages, _ := lru.NewARC(inmemoryPeers)
	knownMessages, _ := lru.NewARC(inmemoryMessages)
	return &FastBFT{
		fastbftEventMux: 	new(event.TypeMux),
		coreStarted:     	false,
		db:              	db,
		logger:          	log.New(),
		handlerWg:       	new(sync.WaitGroup),
		commitCh:        	make(chan *types.Block, 1),
		recentMessages:  	recentMessages,
		knownMessages:   	knownMessages,
		pendingProposals:   prque.New(),
		pendingProposalsMu: new(sync.Mutex),
		pendingVotes:    	prque.New(),
		pendingVotesMu: 	new(sync.Mutex),
		faultNode:			faultNode,
	}
}

// Authorize injects a private key into the consensus engine to mint new blocks
// with.
func (fb *FastBFT) Authorize(signer common.Address) {
	fb.lock.Lock()
	defer fb.lock.Unlock()

	fb.signer = signer
	fb.valSet = NewSet([]common.Address{fb.signer})
}

// Author retrieves the Ethereum address of the account that minted the given
// block, which may be different from the header's coinbase if a consensus
// engine is based on signatures.
func (fb *FastBFT) Author(header *types.Header) (common.Address, error) {
	return header.Coinbase, nil
}

// VerifyHeader checks whether a header conforms to the consensus rules of a
// given engine. Verifying the seal may be done optionally here, or explicitly
// via the VerifySeal method.
func (fb *FastBFT) VerifyHeader(chain consensus.ChainReader, header *types.Header, seal bool) error {
	return fb.verifyHeader(chain, header, nil)
}

// VerifyHeaders is similar to VerifyHeader, but verifies a batch of headers
// concurrently. The method returns a quit channel to abort the operations and
// a results channel to retrieve the async verifications (the order is that of
// the input slice).
func (fb *FastBFT) VerifyHeaders(chain consensus.ChainReader, headers []*types.Header, seals []bool) (chan<- struct{}, <-chan error) {
	abort := make(chan struct{})
	results := make(chan error, len(headers))

	go func() {
		for i, header := range headers {
			err := fb.verifyHeader(chain, header, headers[:i])

			select {
			case <-abort:
				return
			case results <- err:
			}
		}
	}()
	return abort, results
}

// verifyHeader checks whether a header conforms to the consensus rules.The
// caller may optionally pass in a batch of parents (ascending order) to avoid
// looking those up from the database. This is useful for concurrently verifying
// a batch of new headers.
func (fb *FastBFT) verifyHeader(chain consensus.ChainReader, header *types.Header, parents []*types.Header) error {
	if header.Number == nil {
		return errUnknownBlock
	}

	// Don't waste time checking blocks from the future
	if header.Time.Cmp(big.NewInt(now().Unix())) > 0 {
		return consensus.ErrFutureBlock
	}
	return nil
}

// VerifyUncles verifies that the given block's uncles conform to the consensus
// rules of a given engine.
func (fb *FastBFT) VerifyUncles(chain consensus.ChainReader, block *types.Block) error {
	if len(block.Uncles()) > 0 {
		return errInvalidUncleHash
	}
	return nil
}

// VerifySeal checks whether the crypto seal on a header is valid according to
// the consensus rules of the given engine.
func (fb *FastBFT) VerifySeal(chain consensus.ChainReader, header *types.Header) error {
	return fb.verifySeal(chain, header, nil)
}

// verifySeal checks whether the signature contained in the header satisfies the
// consensus protocol requirements. The method accepts an optional list of parent
// headers that aren't yet part of the local blockchain to generate the snapshots
// from.
func (fb *FastBFT) verifySeal(chain consensus.ChainReader, header *types.Header, parents []*types.Header) error {
	// get parent header and ensure the signer is in parent's validator set
	number := header.Number.Uint64()
	if number == 0 {
		return errUnknownBlock
	}

	// ensure that the difficulty equals to defaultDifficulty
	if header.Difficulty.Cmp(defaultDifficulty) != 0 {
		return errInvalidDifficulty
	}

	// ensure the sealer is the leader
	if !fb.valSet.IsLeader(header.Coinbase) {
		return errNotLeader
	}

	return nil
}

// Prepare initializes the consensus fields of a block header according to the
// rules of a particular engine. The changes are executed inline.
func (fb *FastBFT) Prepare(chain consensus.ChainReader, header *types.Header) error {
	// unused fields, force to set to empty
	header.Nonce = emptyNonce

	parent := chain.GetHeader(header.ParentHash, header.Number.Uint64()-1)
	if parent == nil {
		return consensus.ErrUnknownAncestor
	}

	// use the same difficulty for all blocks
	header.Difficulty = defaultDifficulty
	// set header's timestamp
	header.Time = big.NewInt(time.Now().Unix())
	// header.Time = new(big.Int).Add(parent.Time, new(big.Int).SetUint64(1))
	// if header.Time.Int64() < time.Now().Unix() {
	// 	header.Time = big.NewInt(time.Now().Unix())
	// }
	header.Coinbase = fb.signer
	return nil
}

// Finalize runs any post-transaction state modifications (e.g. block rewards)
// and assembles the final block.
// Note: The block header and state database might be updated to reflect any
// consensus rules that happen at finalization (e.g. block rewards).
func (fb *FastBFT) Finalize(chain consensus.ChainReader, header *types.Header, state *state.StateDB, txs []*types.Transaction, uncles []*types.Header, receipts []*types.Receipt) (*types.Block, error) {
	// No block rewards in FastBFT, so the state remains as is and uncles are dropped
	header.Root = state.IntermediateRoot(chain.Config().IsEIP158(header.Number))
	header.UncleHash = nilUncleHash

	// Assemble and return the final block for sealing
	return types.NewBlock(header, txs, nil, receipts), nil
}

// Seal generates a new sealing request for the given input block and pushes
// the result into the given channel.
//
// Note, the method returns immediately and will send the result async. More
// than one result may also be returned depending on the consensus algorithm.
func (fb *FastBFT) Seal(chain consensus.ChainReader, block *types.Block, results chan<- *types.Block, stop <-chan struct{}) error {

	// Refuse to seal empty blocks (no reward but would spin sealing)
	if len(block.Transactions()) == 0 {
		log.Info("Sealing paused, waiting for transactions")
		return nil
	}

	header := block.Header()
	number := header.Number.Uint64()
	parent := chain.GetHeader(header.ParentHash, number-1)
	if parent == nil {
		return consensus.ErrUnknownAncestor
	}
	if fb.faultNode == 1 || fb.faultNode == 2 {
		parentBlock := chain.GetBlock(header.ParentHash, number-1)
		faultBlock := fb.makeBlockWithoutSeal(chain, parentBlock)
		prepare := NewPrepare(faultBlock)
		go fb.EventMux().Post(prepare)
	}

	// delay := time.Unix(header.Time.Int64(), 0).Sub(now())
	go func() {
		// select {
		// case <-time.After(0):
		// case <-stop:
		// 	results <- nil
		// 	return
		// }
		// get the proposed block hash and clear it if the seal() is completed.
		fb.sealMu.Lock()
		fb.proposedBlockHash = block.Hash()

		defer func() {
			fb.proposedBlockHash = common.Hash{}
			fb.sealMu.Unlock()
		}()

		if len(block.Transactions()) > 0 {
			prepare := NewPrepare(block)
			go fb.EventMux().Post(prepare)
		}

		for {
			select {
			case result := <-fb.commitCh:
				// if the block hash and the hash from channel are the same,
				// return the result. Otherwise, keep waiting the next hash.
				if result != nil {
					results <- result
					return
				}
			case <-stop:
				results <- nil
				return
			}
		}
	}()
	return nil
}

// SealHash returns the hash of a block prior to it being sealed.
func (fb *FastBFT) SealHash(header *types.Header) (hash common.Hash) {
	hasher := sha3.NewKeccak256()

	rlp.Encode(hasher, []interface{}{
		header.ParentHash,
		header.UncleHash,
		header.Coinbase,
		header.Root,
		header.TxHash,
		header.ReceiptHash,
		header.Bloom,
		header.Difficulty,
		header.Number,
		header.GasLimit,
		header.GasUsed,
		header.Time,
		header.Extra,
	})
	hasher.Sum(hash[:0])
	return hash
}

// CalcDifficulty is the difficulty adjustment algorithm. It returns the difficulty
// that a new block should have.
func (fb *FastBFT) CalcDifficulty(chain consensus.ChainReader, time uint64, parent *types.Header) *big.Int {
	return new(big.Int)
}

// APIs returns the RPC APIs this consensus engine provides.
func (fb *FastBFT) APIs(chain consensus.ChainReader) []rpc.API {
	return []rpc.API{{
		Namespace: "fastbft",
		Version:   "1.0",
		Service:   &API{chain: chain, fastbft: fb},
		Public:    true,
	}}
}

// Protocol returns the protocol for this consensus
func (fb *FastBFT) Protocol() consensus.Protocol {
	return consensus.Protocol{
		Name:     "fastbft",
		Versions: []uint{64},
		Lengths:  []uint64{22},
	}
}

// Close terminates any background threads maintained by the consensus engine.
func (fb *FastBFT) Close() error {
	return nil
}

// Address return signer
func (fb *FastBFT) Address() common.Address {
	return fb.signer
}

// EventMux return event mux
func (fb *FastBFT) EventMux() *event.TypeMux {
	return fb.fastbftEventMux
}

// StartFastBFT implements consensus.FastBFT.StartFastBFT
func (fb *FastBFT) StartFastBFT(chain consensus.ChainReader, currentBlock func() *types.Block, hasBadBlock func(hash common.Hash) bool) error {
	fb.coreMu.Lock()
	defer fb.coreMu.Unlock()

	if fb.coreStarted {
		return errors.New("started engine")
	}
	// clear previous data
	fb.proposedBlockHash = common.Hash{}
	if fb.commitCh != nil {
		close(fb.commitCh)
	}
	fb.commitCh = make(chan *types.Block, 1)

	fb.chain = chain
	fb.currentBlock = currentBlock
	fb.hasBadBlock = hasBadBlock
	fb.valSet = fb.GetSignersFromGenesis(chain)

	if err := fb.CoreStart(); err != nil {
		return err
	}

	fb.coreStarted = true
	return nil
}

// Stop implements consensus.FastBFT.Stop
func (fb *FastBFT) Stop() error {
	fb.coreMu.Lock()
	defer fb.coreMu.Unlock()
	if !fb.coreStarted {
		return ErrStoppedEngine
	}
	if err := fb.CoreStop(); err != nil {
		return err
	}
	fb.coreStarted = false
	return nil
}

// GetSignersFromGenesis get ValidatorSet from genesis
func (fb *FastBFT) GetSignersFromGenesis(chain consensus.ChainReader) ValidatorSet {
	genesis := chain.GetHeaderByNumber(0)
	if genesis != nil {
		signers := make([]common.Address, (len(genesis.Extra)-extraVanity-extraSeal)/common.AddressLength)
		for i := 0; i < len(signers); i++ {
			copy(signers[i][:], genesis.Extra[extraVanity+i*common.AddressLength:])
		}
		return NewSet(signers)
	}
	return nil
}
// HandleMsg implements consensus.Handler.HandleMsg
func (fb *FastBFT) HandleMsg(addr common.Address, msg p2p.Msg) (bool, error) {
	fb.coreMu.Lock()
	defer fb.coreMu.Unlock()
	if msg.Code == proposalMsg || msg.Code == voteMsg {
		if !fb.coreStarted {
			return true, ErrStoppedEngine
		}
		data, hash, err := fb.decode(msg)
		if err != nil {
			fmt.Println("In fb.decode", err)
			return true, errDecodeFailed
		}

		// Mark peer's message
		ms, ok := fb.recentMessages.Get(addr)
		var m *lru.ARCCache
		if ok {
			m, _ = ms.(*lru.ARCCache)
		} else {
			m, _ = lru.NewARC(inmemoryMessages)
			fb.recentMessages.Add(addr, m)
		}
		m.Add(hash, true)

		// Mark self known message
		if _, ok := fb.knownMessages.Get(hash); ok {
			return true, nil
		}
		fb.knownMessages.Add(hash, true)

		switch msg.Code {
		case proposalMsg:
			var proposal Proposal
			if err := Decode(data, &proposal); err != nil {
				log.Error("invalid rlp.Decode ", "err:", err)
				return true, errDecodeFailed
			}
			go fb.EventMux().Post(proposal)
		case voteMsg:
			var vote Vote
			if err := Decode(data, &vote); err != nil {
				log.Error("invalid rlp.Decode ", "err:", err)
				return true, errDecodeFailed
			}
			go fb.EventMux().Post(vote)
		}
		return true, nil
	}// else if msg.Code == NewBlockMsg {
	//  	return true, nil
	// }
	return false, nil
}

// SetBroadcaster implements consensus.Handler.SetBroadcaster
func (fb *FastBFT) SetBroadcaster(broadcaster consensus.Broadcaster) {
	fb.broadcaster = broadcaster
}

// NewChainHead return nil
func (fb *FastBFT) NewChainHead() error {
	fb.coreMu.RLock()
	defer fb.coreMu.RUnlock()
	if !fb.coreStarted {
		return ErrStoppedEngine
	}
	go fb.EventMux().Post(FinalCommittedEvent{})
	return nil
}

func (fb *FastBFT) makeHeader(parent *types.Block) *types.Header {
	header := &types.Header{
		ParentHash: parent.Hash(),
		Number:     parent.Number().Add(parent.Number(), common.Big1),
		GasLimit:   core.CalcGasLimit(parent, parent.GasLimit(), parent.GasLimit()),
		GasUsed:    0,
		Extra:      parent.Extra(),
		Time:       big.NewInt(time.Now().Unix()),
		Difficulty: defaultDifficulty,
	}
	return header
}

func (fb *FastBFT) makeBlockWithoutSeal(chain consensus.ChainReader, parent *types.Block) *types.Block {
	header := fb.makeHeader(parent)
	fb.Prepare(chain, header)
	state, _, _ := chain.StateAt(parent.Root())
	block, _ := fb.Finalize(chain, header, state, nil, nil, nil)
	return block
}