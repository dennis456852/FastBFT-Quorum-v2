// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"math/big"
	"reflect"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

func makeBlock(number int64) *types.Block {
	header := &types.Header{
		Difficulty: big.NewInt(0),
		Number:     big.NewInt(number),
		GasLimit:   0,
		GasUsed:    0,
		Time:       big.NewInt(0),
	}
	block := &types.Block{}
	return block.WithSeal(header)
}


func TestProposalRLP(t *testing.T) {
	p := &Proposal{
		Proposer: common.HexToAddress("0x1234567890"),
		Height:   big.NewInt(1),
		Round:    big.NewInt(1),
		Block:    makeBlock(1),
	}

	rlpProposal, _ := Encode(p)
	
	var proposal Proposal
	err := Decode(rlpProposal, &proposal)

	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(p.Proposer, proposal.Proposer) {
		t.Errorf("Proposer mismatch: have %v", proposal.Proposer)
		t.Errorf("Proposer mismatch: want %v", p.Proposer)
	}
	if !reflect.DeepEqual(p.Height, proposal.Height) {
		t.Errorf("Height mismatch: have %v", proposal.Height)
		t.Errorf("Height mismatch: want %v", p.Height)
	}
	if !reflect.DeepEqual(p.Round, proposal.Round) {
		t.Errorf("Round mismatch: have %v", proposal.Round)
		t.Errorf("Round mismatch: want %v", p.Round)
	}
	if !reflect.DeepEqual(p.Block, proposal.Block) {
		t.Errorf("Block mismatch: have %v", proposal.Block)
		t.Errorf("Block mismatch: want %v", p.Block)
	}
}

func TestVoteRLP(t *testing.T) {
	p := NewProposal(common.HexToAddress("0x1234567890"), big.NewInt(1), big.NewInt(1), makeBlock(1))

	v := &Vote{
		Voter:    common.HexToAddress("0x1234567890"),
		Proposal: p,
	}

	rlpVote, _ := Encode(v)
	
	var vote *Vote
	err := Decode(rlpVote, &vote)  
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(v.Voter, vote.Voter) {
		t.Errorf("Voter mismatch: have %v", vote.Voter)
		t.Errorf("Voter mismatch: want %v", v.Voter)
	}
	if !reflect.DeepEqual(v.Proposal, vote.Proposal) {
		t.Errorf("Proposal mismatch: have %v", vote.Proposal)
		t.Errorf("Proposal mismatch: want %v", v.Proposal)
	}
}
