// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/log"
)

// Lockset struct
type Lockset struct {
	Votes 	[]Vote
	Counter map[common.Hash]int
	Blocks 	map[common.Hash]*types.Block
}

// NewLockset creates a new lockset
func NewLockset() *Lockset {
	return &Lockset{
		Votes: make([]Vote, 0),
		Counter: make(map[common.Hash]int),
		Blocks: make(map[common.Hash]*types.Block),
	}
}

// Len is the length of votes in Lockset
func (ls *Lockset) Len() int {
	return len(ls.Votes)
}

// AddVote add vote to Lockset
func (ls *Lockset) AddVote(v Vote) {
	ls.Votes = append(ls.Votes, v)
	ls.Blocks[v.Proposal.Block.Hash()] = v.Proposal.Block
	if _, exist := ls.Counter[v.Proposal.Block.Hash()]; exist {
		ls.Counter[v.Proposal.Block.Hash()]++
	} else {
		ls.Counter[v.Proposal.Block.Hash()]=1
	}
}

// Clear remove all votes in Lockset
func (ls *Lockset) Clear() {
	ls.Votes = make([]Vote, 0)
	ls.Counter = make(map[common.Hash]int)
	ls.Blocks = make(map[common.Hash]*types.Block)
}

// Copy Lockset
func (ls *Lockset) Copy() *Lockset {
	nls := NewLockset()
	nls.Votes = ls.Votes
	nls.Counter = ls.Counter
	nls.Blocks = ls.Blocks
	return nls
}

// IsValid checks the lockset is valid lockset
func (fb *FastBFT) IsValid(ls *Lockset) bool {
	if ls.Len() > fb.valSet.Size()*4/5 {
		log.Info("Lockset is valid", "vote", ls.Len())
		return true
	}
	log.Info("Lockset is not valid", "vote", ls.Len())
	return false
}

// IsBlockLocked checks the previous lockset has locked block
func (fb *FastBFT) IsBlockLocked(ls *Lockset) (bool, *types.Block) {
	var maxHash 	common.Hash
	var maxVotes	int
	for k, v := range ls.Counter {
		if (maxHash == common.Hash{}) || v > maxVotes {
			maxHash = k
			maxVotes = v
		}
	}
	// more than n - 3f votes
	if maxVotes > fb.valSet.Size()*2/5 && !fb.isEmptyBlock(ls.Blocks[maxHash]) {
		return true, ls.Blocks[maxHash]
	}
	return false, nil
}

// HasQuorum checks the votes for block in lockset reach Quorum size
func (fb *FastBFT) HasQuorum(ls *Lockset) (bool, *types.Block){
	var maxHash 	common.Hash
	var maxVotes	int
	for k, v := range ls.Counter {
		if (maxHash == common.Hash{}) || v > maxVotes {
			maxHash = k
			maxVotes = v
		}
	}
	// more than n - f votes
	if maxVotes > fb.valSet.Size()*4/5 && !fb.isEmptyBlock(ls.Blocks[maxHash]) {
		return true, ls.Blocks[maxHash]
	}
	return false, nil
}