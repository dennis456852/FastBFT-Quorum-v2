// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/log"
)

// Proposal struct
type Proposal struct {
	Proposer common.Address
	Height   *big.Int
	Round    *big.Int
	Block    *types.Block
}

// NewProposal creates a new proposal
func NewProposal(proposer common.Address, height *big.Int, round *big.Int, block *types.Block) Proposal {
	return Proposal{
		Proposer: proposer,
		Height:   height,
		Round:    round,
		Block:    block,
	}
}

// Copy Proposal
// func (p *Proposal) Copy() Proposal {
// 	return Proposal{
// 		Proposer: p.Proposer,
// 		Height:   p.Height,
// 		Round:    p.Round,
// 		Block:    p.Block,
// 	}
// }

// check proposal state
// return errInvalidMessage if the message is invalid
// return errFutureMessage if the sequence of proposal is larger than current sequence
// return errOldMessage if the sequence of proposal is smaller than current sequence
func (fb *FastBFT) checkProposalMsg(proposal *Proposal) error {
	if proposal.Block == nil {
		return errInvalidMessage
	}

	if cmpH := fb.currentHeight.Cmp(proposal.Height); cmpH > 0 {
		return errOldMessage
	} else if cmpH < 0 {
		return errFutureMessage
	} else if cmpR := fb.currentRound.Cmp(proposal.Round); cmpR > 0 {
		return errOldMessage
	} else if cmpR < 0 {
		return errFutureMessage
	} else {
		return nil
	}
}

func (fb *FastBFT) storeProposalMsg(proposal *Proposal) {

	log.Info("Store future request", "Height", proposal.Height, "Round", proposal.Round, "hash", proposal.Block.Hash())

	fb.pendingProposalsMu.Lock()
	defer fb.pendingProposalsMu.Unlock()

	fb.pendingProposals.Push(proposal, float32(-proposal.Height.Int64()))
}

func (fb *FastBFT) processPendingProposal() {
	fb.pendingProposalsMu.Lock()
	defer fb.pendingProposalsMu.Unlock()

	for !(fb.pendingProposals.Empty()) {
		m, prio := fb.pendingProposals.Pop()
		p, ok := m.(*Proposal)
		if !ok {
			log.Warn("Malformed request, skip", "msg", m)
			continue
		}
		// Push back if it's a future message
		err := fb.checkProposalMsg(p)
		if err != nil {
			if err == errFutureMessage {
				log.Trace("Stop processing request", "Height", p.Height, "Round", p.Round, "hash", p.Block.Hash())
				fb.pendingProposals.Push(m, prio)
				break
			}
			log.Trace("Skip the pending request", "Height", p.Height, "Round", p.Round, "hash", p.Block.Hash(), "err", err)
			continue
		}
		log.Trace("Post pending request", "Height", p.Height, "Round", p.Round, "hash", p.Block.Hash())

		go fb.EventMux().Post(p)
	}
}