// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/log"
)

const (
	// fetcherID is the ID indicates the block is from FastBFT engine
	fetcherID = "fastbft"
)

func (fb *FastBFT) handleEvents() {
	log.Info("handle events")
	// Clear state
	defer func() {
		// fb.current = nil
		fb.handlerWg.Done()
	}()

	fb.handlerWg.Add(1)

	for {
		select {
		case event, ok := <-fb.events.Chan():
			if !ok {
				return
			}
			// A real event arrived, process interesting content
			switch ev := event.Data.(type) {
			case Prepare:
				fb.handlePrepare(ev)
			case Proposal:
				err := fb.handleProposal(ev)
				p := &Proposal{
					Proposer: ev.Proposer,
					Height: ev.Height,
					Round: ev.Round,
					Block: ev.Block,
				}
				if err == errFutureMessage {
					fb.storeProposalMsg(p)
				}
				fb.processPendingVote()
			case Vote:
				err := fb.handleVote(ev)
				v := &Vote{
					Voter: ev.Voter,
					Proposal: ev.Proposal,
				}
				if err == errFutureMessage {
					fb.storeVoteMsg(v)
				}
			}
		case event, ok := <-fb.timeoutSub.Chan():
			if !ok {
				return
			}
			fb.handleTimeoutMsg(event.Data.(timeoutEvent).timeoutType, event.Data.(timeoutEvent).round)
		case event, ok := <-fb.finalCommittedSub.Chan():
			if !ok {
				return
			}
			switch event.Data.(type) {
			case FinalCommittedEvent:
				fb.handleFinalCommitted()
			}
		}
	}
}

// handle Prepare message
func (fb *FastBFT) handlePrepare(prepare Prepare) error {
	if prepare.Block.GasUsed() == 0 {
		fb.currentPrepareFault = prepare
		log.Info("PrepareFaultBlock")
	}
	if fb.currentPrepare.Block == nil {
		fb.currentPrepare = prepare
		log.Info("handlePrepare")
		fb.sendProposal(fb.currentRound)
	}
	return nil
}

// handle Proposal message
func (fb *FastBFT) handleProposal(proposal Proposal) error {
	log.Info("Receive Proposal", "hash", proposal.Block.Hash())
	if err := fb.checkProposalMsg(&proposal); err != nil {
		if err == errInvalidMessage {
			log.Warn("invalid proposal")
			return err
		}
		log.Warn("unexpected proposal", "err", err)
		return err
	}
	fb.stopVoteTimer()
	fb.currentProposal = proposal
	vote := NewVote(fb.signer, proposal)
	if fb.faultNode == 2 {
		err := fb.FaultBroadcast(fb.valSet, voteMsg, vote, fb.makeFaultVote())
		return err
	}
	err := fb.Broadcast(fb.valSet, voteMsg, vote)
	return err
}

// handle Vote message
func (fb *FastBFT) handleVote(vote Vote) error {
	log.Info("Receive Vote", "hash", vote.Proposal.Block.Hash())
	if err := fb.checkVoteMsg(&vote); err != nil {
		if err == errInvalidMessage {
			log.Warn("invalid vote")
			return err
		}
		log.Warn("unexpected vote", "err", err)
		return err
	}
	fb.lockset.AddVote(vote)
	if ok, block := fb.HasQuorum(fb.lockset); ok {
		log.Info("Has Quorum", "block", block.Hash())
		fb.previosLockset = fb.lockset.Copy()
		fb.lockset.Clear()
		fb.commit(block)
		return nil
	}
	return nil
}

func (fb *FastBFT) commit(block *types.Block) error {
	if fb.proposedBlockHash == block.Hash() {
		// feed block hash to Seal() and wait the Seal() result
		fb.commitCh <- block
		return nil
	}
	if fb.broadcaster != nil {
		fb.broadcaster.Enqueue(fetcherID, block)
	}
	return nil
}

// t=1 => vote timeout, t=2 => commit timeout
func (fb *FastBFT) handleTimeoutMsg(t int, r uint64) error {
	// vote timeout, round = 0
	if t == 1 && r == 0 {
		log.Info("Vote Timeout")
		vote := NewVote(fb.signer, NewProposal(common.Address{0x00}, fb.currentHeight, fb.currentRound, makeEmptyBlock()))
		err := fb.Broadcast(fb.valSet, voteMsg, vote)
		return err
	// vote timeout, round > 0
	} else if t == 1 && r > 0 {
		log.Info("Vote Timeout")
		// empty block is in current proposal
		if fb.currentProposal.Block == nil || fb.isEmptyBlock(fb.currentProposal.Block) {
			vote := NewVote(fb.signer, NewProposal(common.Address{0x00}, fb.currentHeight, fb.currentRound, makeEmptyBlock()))
			err := fb.Broadcast(fb.valSet, voteMsg, vote)
			return err
		}
		vote := NewVote(fb.signer, NewProposal(fb.currentProposal.Proposer, fb.currentHeight, fb.currentRound, fb.currentProposal.Block))
		err := fb.Broadcast(fb.valSet, voteMsg, vote)
		return err
	// commit timeout
	} else if t == 2 {
		log.Info("Commit Timeout")
		nextRound := big.NewInt(0).Add(big.NewInt(int64(r)), big.NewInt(1))
		fb.previosLockset = fb.lockset.Copy()
		fb.startNewRound(nextRound)
	}
	return nil
}

func (fb *FastBFT) handleFinalCommitted() error {
	fb.currentPrepare = Prepare{}
	fb.currentProposal = Proposal{}
	fb.startNewRound(common.Big0)
	return nil
}

func makeEmptyBlock() *types.Block {
	header := &types.Header{
		Difficulty: common.Big0,
		Number:     common.Big0,
		GasLimit:   0,
		GasUsed:    0,
		Time:       common.Big0,
	}
	block := &types.Block{}
	return block.WithSeal(header)
}

func (fb *FastBFT) isEmptyBlock(block *types.Block) bool {
	if block.Number().Cmp(common.Big0) == 0 {
		return true
	}
	return false
}

func (fb *FastBFT) makeFaultProposal() Proposal {
	return NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepareFault.Block)
}

func (fb *FastBFT) makeFaultVote() Vote {
	return NewVote(fb.signer, NewProposal(fb.signer, fb.currentHeight, fb.currentRound, fb.currentPrepareFault.Block))
}
