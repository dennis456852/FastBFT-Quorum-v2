// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"crypto/rand"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/log"
)

// Message Code here
const (
	proposalMsg = 0x11
	voteMsg     = 0x12
	NewBlockMsg = 0x07
)

// Broadcast sends message to self and other nodes
func (fb *FastBFT) Broadcast(valSet ValidatorSet, msgCode uint64, message interface{}) error {
	hash := RLPHash(message)
	fb.knownMessages.Add(hash, true)
	log.Info("Broadcast message", "msgCode", msgCode)
	if msgCode == proposalMsg {
		// to others
		msg := message.(Proposal)
		encodedMessage, err := Encode(&msg)
		if err != nil {
			log.Error("Failed to encode Proposal", "err", err)
			return err
		}
		if fb.broadcaster != nil {
			ps := fb.broadcaster.FindAllPeers()
			for _, p := range ps {
				go p.Send(msgCode, encodedMessage)
			}
		}
		// to self
		go fb.EventMux().Post(message.(Proposal))
	} else if msgCode == voteMsg {
		// to others
		msg := message.(Vote)
		encodedMessage, err := Encode(&msg)
		if err != nil {
			log.Error("Failed to encode Vote", "err", err)
			return err
		}

		if fb.broadcaster != nil {
			ps := fb.broadcaster.FindAllPeers()
			for _, p := range ps {
				go p.Send(msgCode, encodedMessage)
			}
		}
		// to self
		go fb.EventMux().Post(message.(Vote))
	}
	return nil
}

// FaultBroadcast sends message to self and other nodes with differnet message
func (fb *FastBFT) FaultBroadcast(valSet ValidatorSet, msgCode uint64, message1 interface{}, message2 interface{}) error {
	log.Info("In FaultBroadcast")
	hash := RLPHash(message1)
	fb.knownMessages.Add(hash, true)
	log.Info("Broadcast message", "msgCode", msgCode)
	if msgCode == proposalMsg {
		// to others
		msg1 := message1.(Proposal)
		msg2 := message2.(Proposal)
		encodedMessage1, err := Encode(&msg1)
		encodedMessage2, err := Encode(&msg2)
		if err != nil {
			log.Error("Failed to encode Proposal", "err", err)
			return err
		}
		if fb.broadcaster != nil {
			ps := fb.broadcaster.FindAllPeers()
			for _, p := range ps {
				result, _ := rand.Int(rand.Reader, common.Big2)
				if result.Cmp(common.Big0) == 0 {
					// send correct message
					go p.Send(msgCode, encodedMessage1)
				} else {
					// send faulty message
					go p.Send(msgCode, encodedMessage2)
				}
			}
		}
		// to self
		go fb.EventMux().Post(message2.(Proposal))
	} else if msgCode == voteMsg {
		// to others
		msg1 := message1.(Vote)
		msg2 := message2.(Vote)
		encodedMessage1, err := Encode(&msg1)
		encodedMessage2, err := Encode(&msg2)
		if err != nil {
			log.Error("Failed to encode Vote", "err", err)
			return err
		}

		if fb.broadcaster != nil {
			ps := fb.broadcaster.FindAllPeers()
			for _, p := range ps {
				result, _ := rand.Int(rand.Reader, common.Big2)
				if result.Cmp(common.Big0) == 0 {
					// send correct message
					go p.Send(msgCode, encodedMessage1)
				} else {
					// send faulty message
					go p.Send(msgCode, encodedMessage2)
				}
			}
		}
		// to self
		go fb.EventMux().Post(message2.(Vote))
	}
	return nil
}