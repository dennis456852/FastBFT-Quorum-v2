// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"math"
	"math/big"
	"reflect"
	"sort"
	"sync"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/log"
)

type defaultValidator struct {
	address common.Address
}

func newdefaultValidator(addr common.Address) Validator {
	return &defaultValidator{
		address: addr,
	}
}

func (val *defaultValidator) Address() common.Address {
	return val.address
}

func (val *defaultValidator) String() string {
	return val.Address().String()
}

// ----------------------------------------------------------------------------

type defaultSet struct {
	validators	Validators

	leader      Validator
	validatorMu sync.RWMutex
}

func newDefaultSet(addrs []common.Address) *defaultSet {
	valSet := &defaultSet{}

	// init validators
	valSet.validators = make([]Validator, len(addrs))
	for i, addr := range addrs {
		valSet.validators[i] = newdefaultValidator(addr)
	}
	// sort validator
	sort.Sort(valSet.validators)
	// init leader
	if valSet.Size() > 0 {
		valSet.leader = valSet.GetByIndex(0)
	}

	return valSet
}

// NewSet creates a new ValidatorSet
func NewSet(addrs []common.Address) ValidatorSet {
	return newDefaultSet(addrs)
}
func (valSet *defaultSet) Size() int {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()
	return len(valSet.validators)
}

func (valSet *defaultSet) List() []Validator {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()
	return valSet.validators
}

func (valSet *defaultSet) GetByIndex(i uint64) Validator {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()
	if i < uint64(valSet.Size()) {
		return valSet.validators[i]
	}
	return nil
}

func (valSet *defaultSet) GetByAddress(addr common.Address) (int, Validator) {
	for i, val := range valSet.List() {
		if addr == val.Address() {
			return i, val
		}
	}
	return -1, nil
}

func (valSet *defaultSet) GetLeader() Validator {
	return valSet.leader
}

func (valSet *defaultSet) IsLeader(address common.Address) bool {
	_, val := valSet.GetByAddress(address)
	return reflect.DeepEqual(valSet.GetLeader(), val)
}

func (valSet *defaultSet) CalcLeader(lastLeader common.Address, round *big.Int) {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()
	valSet.leader = roundRobinLeader(valSet, lastLeader, round.Uint64())
	log.Info("Assign Leader", "address", valSet.leader.Address(), "size", valSet.Size())
}

func calcSeed(valSet *defaultSet, proposer common.Address, round uint64) uint64 {
	offset := 0
	if idx, val := valSet.GetByAddress(proposer); val != nil {
		offset = idx
	}
	return uint64(offset) + round
}

func emptyAddress(addr common.Address) bool {
	return addr == common.Address{}
}

func roundRobinLeader(valSet *defaultSet, leader common.Address, round uint64) Validator {
	if valSet.Size() == 0 {
		return nil
	}
	seed := uint64(0)
	if emptyAddress(leader) {
		seed = round
	} else {
		seed = calcSeed(valSet, leader, round) + 1
	}
	pick := seed % uint64(valSet.Size())
	return valSet.GetByIndex(pick)
}

func (valSet *defaultSet) UpdateLeader(round *big.Int) {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()
	pick := round.Uint64() % uint64(valSet.Size())
	valSet.leader = valSet.GetByIndex(pick)
	log.Info("Assign Leader", "address", valSet.leader.Address())
}

func (valSet *defaultSet) AddValidator(address common.Address) bool {
	valSet.validatorMu.Lock()
	defer valSet.validatorMu.Unlock()
	for _, v := range valSet.validators {
		if v.Address() == address {
			return false
		}
	}
	log.Info("Add New Validator", "address", address.Hex())
	valSet.validators = append(valSet.validators, newdefaultValidator(address))
	// sort validator
	sort.Sort(valSet.validators)
	return true
}

func (valSet *defaultSet) RemoveValidator(address common.Address) bool {
	valSet.validatorMu.Lock()
	defer valSet.validatorMu.Unlock()

	for i, v := range valSet.validators {
		if v.Address() == address {
			valSet.validators = append(valSet.validators[:i], valSet.validators[i+1:]...)
			return true
		}
	}
	return false
}

func (valSet *defaultSet) Copy() ValidatorSet {
	valSet.validatorMu.RLock()
	defer valSet.validatorMu.RUnlock()

	addresses := make([]common.Address, 0, len(valSet.validators))
	for _, v := range valSet.validators {
		addresses = append(addresses, v.Address())
	}
	return newDefaultSet(addresses)
}

func (valSet *defaultSet) F() int { return int(math.Ceil(float64(valSet.Size()-1) / 5)) }
