// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/consensus"
)

// API is a user facing RPC API to dump FastBFT state
type API struct {
	chain   consensus.ChainReader
	fastbft *FastBFT
}

// GetEtherbase gets the signer
func (api *API) GetEtherbase() common.Address {
	return api.fastbft.signer
}

// Validators returns the current validators.
func (api *API) Validators() []common.Address {
	var validators []common.Address
	for _, val := range api.fastbft.valSet.List() {
		validators = append(validators, val.Address())
	}
	return validators
}
