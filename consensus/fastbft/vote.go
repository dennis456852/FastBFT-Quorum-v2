// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/log"
)

// Vote struct
type Vote struct {
	Voter    common.Address
	Proposal Proposal
}

// NewVote creates a new vote
func NewVote(voter common.Address, proposal Proposal) Vote {
	return Vote{
		Voter:    voter,
		Proposal: proposal,
	}
}

// check vote state
// return errInvalidMessage if the message is invalid
// return errFutureMessage if the sequence of vote is larger than current sequence
// return errOldMessage if the sequence of vote is smaller than current sequence
func (fb *FastBFT) checkVoteMsg(vote *Vote) error {
	if vote.Proposal.Block == nil {
		return errInvalidMessage
	}
	if cmpH := fb.currentHeight.Cmp(vote.Proposal.Height); cmpH > 0 {
		return errOldMessage
	} else if cmpH < 0 {
		return errFutureMessage
	} else if cmpR := fb.currentRound.Cmp(vote.Proposal.Round); cmpR > 0 {
		return errOldMessage
	} else if cmpR < 0 {
		return errFutureMessage
	} else {
		return nil
	}
}

func (fb *FastBFT) storeVoteMsg(vote *Vote) {

	log.Info("Store future request", "Height", vote.Proposal.Height, "Round", vote.Proposal.Round, "hash", vote.Proposal.Block.Hash())

	fb.pendingVotesMu.Lock()
	defer fb.pendingVotesMu.Unlock()

	fb.pendingVotes.Push(vote, float32(-vote.Proposal.Height.Int64()))
}

func (fb *FastBFT) processPendingVote() {
	fb.pendingVotesMu.Lock()
	defer fb.pendingVotesMu.Unlock()

	for !(fb.pendingVotes.Empty()) {
		m, prio := fb.pendingVotes.Pop()
		v, ok := m.(*Vote)
		if !ok {
			log.Warn("Malformed request, skip", "msg", m)
			continue
		}
		// Push back if it's a future message
		err := fb.checkVoteMsg(v)
		if err != nil {
			if err == errFutureMessage {
				log.Trace("Stop processing request", "Height", v.Proposal.Height, "Round", v.Proposal.Round, "hash", v.Proposal.Block.Hash())
				fb.pendingVotes.Push(m, prio)
				break
			}
			log.Trace("Skip the pending request", "Height", v.Proposal.Height, "Round", v.Proposal.Round, "hash", v.Proposal.Block.Hash(), "err", err)
			continue
		}
		log.Trace("Post pending request", "Height", v.Proposal.Height, "Round", v.Proposal.Round, "hash", v.Proposal.Block.Hash())

		go fb.EventMux().Post(v)
	}
}