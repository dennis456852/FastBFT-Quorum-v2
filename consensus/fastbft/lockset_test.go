// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package fastbft

import (
	"fmt"
	"testing"

	"github.com/ethereum/go-ethereum/common"
)

func TestLocksetCopy(t *testing.T) {
	var pls *Lockset
	ls := NewLockset()
	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567890"), common.Big1, common.Big0, makeBlock(1))))
	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567891"), common.Big1, common.Big0, makeBlock(1))))
	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567892"), common.Big1, common.Big0, makeBlock(1))))
	
	pls = ls.Copy()

	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567893"), common.Big1, common.Big0, makeBlock(1))))
	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567894"), common.Big1, common.Big0, makeBlock(1))))
	ls.AddVote(NewVote(common.HexToAddress("0x1234567890"), NewProposal(common.HexToAddress("0x1234567895"), common.Big1, common.Big0, makeBlock(1))))

	fmt.Println(ls.Len())
	fmt.Println(pls.Len())
}